package com.advancable.mercenaryrevolution.client;

import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                THEGame.setAssetsPath("android\\assets\\");
                THEGame.Language="eng";
                return new GwtApplicationConfiguration(480, 320);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return THEGame.getInstance();

        }

        @Override
        public ApplicationListener createApplicationListener() {
                return null;
        }
}