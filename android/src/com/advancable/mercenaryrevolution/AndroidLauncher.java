package com.advancable.mercenaryrevolution;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useCompass=false;
		config.useAccelerometer=false;
		config.useImmersiveMode=false;
		config.useWakelock=false;
		THEGame.setAssetsPath("");
		THEGame.Language="eng";
		initialize(THEGame.getInstance(), config);

	}
}
