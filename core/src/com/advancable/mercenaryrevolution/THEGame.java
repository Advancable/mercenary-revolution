package com.advancable.mercenaryrevolution;

import com.advancable.mercenaryrevolution.Model.Data;
import com.advancable.mercenaryrevolution.Model.Match;
import com.advancable.mercenaryrevolution.Model.Screens.Battle_Screen;
import com.advancable.mercenaryrevolution.Model.Screens.Help_Screen;
import com.advancable.mercenaryrevolution.Model.Screens.Load_Game_Selection_Screen;
import com.advancable.mercenaryrevolution.Model.Screens.Menu_Screen;
import com.advancable.mercenaryrevolution.Model.Screens.New_Game_Selection_Screen;
import com.advancable.mercenaryrevolution.Model.Screens.Options_Screen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Main game class.
 */
public class THEGame extends Game {
    public static String Language;
    public static float ppuX, ppuY;
    public static FreeTypeFontGenerator Cantarell_Bold_Generator;
    public static FreeTypeFontGenerator Cantarell_Regular_Generator;
    public static Menu_Screen menuScreen;
    public static New_Game_Selection_Screen newGameSelectionScreen;
    public static Battle_Screen battleScreen;
    public static Load_Game_Selection_Screen loadGameSelectionScreen;
    public static Options_Screen optionsScreen;
    public static Help_Screen helpScreen;
    private static ShapeRenderer shape;
    private static SpriteBatch batch;
    protected Screen screen;
    private static THEGame ourInstance = new THEGame();
    private static String assetsPath = "";
    private static Match currentMatch;
    private static Texture buttons;

    private THEGame() {
    }

    public static THEGame getInstance() {
        return ourInstance;
    }

    public static Match getCurrentMatch() {
        return currentMatch;
    }

    public static void setCurrentMatch(Match currentMatch) {
        THEGame.currentMatch = currentMatch;
    }

    public static String getAssetsPath() {
        return assetsPath;
    }

    public static void setAssetsPath(String assetsPath) {
        THEGame.assetsPath = assetsPath;
    }

    public static Texture getButtonsTexture() {
        return buttons;
    }

    @Override
    public void create() {
        Cantarell_Bold_Generator = new FreeTypeFontGenerator(Gdx.files.internal(THEGame.getAssetsPath() +"fonts\\Cantarell-Bold.ttf"));
        Cantarell_Regular_Generator = new FreeTypeFontGenerator(Gdx.files.internal(THEGame.getAssetsPath() +"fonts\\Cantarell-Regular.ttf"));
        batch = new SpriteBatch();
        shape = new ShapeRenderer();
        ppuX = (float) Gdx.graphics.getWidth() / 1366f;
        ppuY = (float) Gdx.graphics.getHeight() / 768f;
        buttons = new Texture(String.format("%s%s%s%s", THEGame.getAssetsPath(), "textures\\", THEGame.Language, "\\Buttons.png"));
        menuScreen = new Menu_Screen(batch);
        battleScreen = new Battle_Screen(batch);
        newGameSelectionScreen = new New_Game_Selection_Screen(batch);
        loadGameSelectionScreen = new Load_Game_Selection_Screen(batch);
        optionsScreen = new Options_Screen(batch);
        helpScreen = new Help_Screen(batch);
        setScreen(menuScreen);
        Music bgm = Gdx.audio.newMusic(Gdx.files.internal(getAssetsPath() + "music\\menu.mp3"));
        bgm.setLooping(true);
        bgm.play();
        Data.loadFonts();
    }

    public ShapeRenderer getShape() {
        return shape;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public void ShowMenu() {
        setScreen(menuScreen);
    }

    public void ShowHelp() {
        setScreen(helpScreen);
    }

    public void ShowNewGameSelection() {
        setScreen(newGameSelectionScreen);
    }

    public void ShowLoadGameSelection() {
        setScreen(loadGameSelectionScreen);
    }

    public void ShowOptions() {
        setScreen(optionsScreen);
    }
}

