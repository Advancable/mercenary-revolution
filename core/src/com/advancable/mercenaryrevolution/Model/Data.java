package com.advancable.mercenaryrevolution.Model;

import com.advancable.mercenaryrevolution.Model.entity.Bullet;
import com.advancable.mercenaryrevolution.Model.entity.Weapon;
import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.io.File;

/**
 * Class that holds the game data, loaded before the start.
 */
public class Data {
    private static Texture Icons;
    private static BitmapFont unitNameFont,unitStatsFont;
    private static Bullet[] bullets=new Bullet[]{};
    private static Weapon[] weapons = new Weapon[]{};
    private static String[] names = new String[]{"Alligator", "Alpaca", "Anaconda", "Antelope", "Wild", "Ram", "Asp", "Baboon", "Sheep", "Badger", "Snipe", "Squirrel", "Bison", "Beaver", "Chipmunk", "Bull", "Camel", "Wolf", "Otter", "Desman", "Viper", "Gazelle", "Gecko", "Cheetah", "Gibbon", "Hyena", "Hippopotamus", "Gnu", "Gorilla", "Grizzly", "Goose", "Porcupine", "Dingo", "Dinosaur", "Raccoon", "Echidna", "Hedgehog", "Toad", "Giraffe", "Hare", "Zebra", "Snake", "Aurochs", "Iguana", "Turkey", "Snow", "Leopard", "Wild", "Oar", "Cayman", "Spurdog", "Cachalot", "Tree", "Rog", "Kangaroo", "Whale", "Cobra", "Koala", "Goat", "Coyote", "Horse", "Cow", "Tomcat", "Cat", "Crocodile", "Rabbit", "Mole", "Rat", "Mallard", "Marten", "Hen", "Lama", "Rock", "Fallow", "Eer", "Least", "Easel", "Lion", "Lemming", "Lemur", "Sloth", "Leopard", "Fox", "Lobster", "Elk", "Horse", "Frog", "Macaque", "Mammoth", "Mongoose", "Baboon", "Marmoset", "Bear", "Mollusk", "Walrus", "Cavy", "Anteater", "Mouse", "Rhea", "Narwhal", "Rhino", "Nutria", "Monkey", "Sheep", "Okapi", "Deer", "Lobster", "Muskrat", "Opossum", "Donkey", "Octopus", "Baboon", "Panther", "Arctic", "Fox", "Rooster", "Penguin", "Puma", "Crayfish", "Wolverene", "Lynx", "Saiga", "Salamander", "Pig", "Serval", "Chamois", "Skunk", "Dog", "Sable", "Marmot", "Suslik", "Tapir", "Tiger", "Seal", "Boa", "Grass", "Nake", "Duck", "Platypus", "Chameleon", "Hamster", "Polecat", "Tortoise", "Chimpanzee", "Chinchilla", "Jaguar", "Yak", "Lizard"};
    public static void loadFonts(){
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = (int) (24 * THEGame.ppuX);
        unitNameFont = THEGame.Cantarell_Bold_Generator.generateFont(parameter);
        unitNameFont.setColor(Color.WHITE);
        unitStatsFont = THEGame.Cantarell_Bold_Generator.generateFont(parameter);
        unitStatsFont.setColor(Color.CYAN);
    }
    public static BitmapFont getUnitNameFont(){
        return unitNameFont;
    }
    public static Texture getUnitIcons(){
        return new Texture(String.format("%s\\textures\\UnitIcons.png",THEGame.getAssetsPath()));
    }
    public static BitmapFont getUnitStatsFont(){
        return unitStatsFont;
    }
    private void loadAmmo(){
        File ammoFile=new File(THEGame.getAssetsPath() +"Ammo.txt");

    }
    private void loadWeapons(){}
    public void set(){
        loadWeapons();
        loadAmmo();
    }
    public static String getRandomName(){
        return names[(int) (Math.random() * names.length - 1)];
    }
}
