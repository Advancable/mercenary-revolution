package com.advancable.mercenaryrevolution.Model.entity;

import com.advancable.mercenaryrevolution.Model.enums.WeaponType;

/**
 * Gun with statistics that can be given to soldiers.
 */
public class Weapon {

    public int idlePhases,hitPhases,reloadPhases,shotPhases, clip,maxClipSize, accuracy;
    public Bullet ammo;
    public String name;
    public float rareness,reloadTime,shotTime;
    public WeaponType weaponType;
    public boolean suppressed,visibleReload;

    public Weapon(String name,WeaponType weaponType,float rareness, int accuracy,int maxClipSize,int idlePhases,int hitPhases,int shotPhases,int reloadPhases,float shotTime,float reloadTime,boolean visibleReload, Bullet ammo , boolean suppressed) {
        this.name = name;
        this.weaponType=weaponType;
        this.rareness=rareness;
        this.accuracy = accuracy;
        this.maxClipSize = maxClipSize;
        this.idlePhases=idlePhases;
        this.hitPhases=hitPhases;
        this.shotPhases=shotPhases;
        this.reloadPhases=reloadPhases;
        this.shotTime =shotTime;
        this.reloadTime =reloadTime;
        this.clip=0;
        this.ammo=ammo;
        this.suppressed = suppressed;
        this.visibleReload=visibleReload;
    }

}
