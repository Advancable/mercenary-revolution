package com.advancable.mercenaryrevolution.Model.entity;

import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * @author Admin
 * Type of bullet that can be loaded into weapons.
 */

public class Bullet {
    public int dmgHP;
    public int dmgPsyche;
    public String name;
    public Animation missAnimation, hitAnimation;
    /**
     * Downloads an elements of bullet miss animation and joins them into a full animation.
     * Packs a miss animation into 'missAnimation' variable.
     * @param missPhases - number of slides in animation
     */
    public void addMissAnimation(int missPhases){
        Texture MissTexture = new Texture(String.format("%s%s%s%s", THEGame.getAssetsPath(), "bullets\\animation\\miss\\", this.name, ".png"));
        TextureRegion[] missFrames = new TextureRegion[missPhases];
        for (int i = 0; i < missPhases; i++)
            missFrames[i] = new TextureRegion(MissTexture, 0, 30 * i, 295, 30);
        this.missAnimation = new Animation(0.1f / missPhases, missFrames);
        missAnimation.setPlayMode(Animation.PlayMode.NORMAL);

    }
    /**
     * Downloads an elements of bullet hit animation and joins them into a full animation.
     * Packs a hit animation into 'hitAnimation' variable.
     * @param hitPhases - number of slides in animation
     */
    public void addHitAnimation(int hitPhases){
        Texture HitTexture = new Texture(String.format("%s%s%s%s", THEGame.getAssetsPath(), "bullets\\animation\\hit\\", this.name, ".png"));
        TextureRegion[] hitFrames = new TextureRegion[hitPhases];
        for (int i = 0; i < hitPhases; i++)
            hitFrames[i] = new TextureRegion(HitTexture, 0, 30 * i, 295, 30);
        this.hitAnimation = new Animation(0.1f / hitPhases, hitFrames);
        hitAnimation.setPlayMode(Animation.PlayMode.NORMAL);
    }
    public Bullet(String name, int dmgHP, int dmgPsyche, int missPhases, int hitPhases) {
        this.name = name;
        this.dmgHP = dmgHP;
        this.dmgPsyche = dmgPsyche;
        this.addMissAnimation(missPhases);
        this.addHitAnimation(hitPhases);
    }
}
