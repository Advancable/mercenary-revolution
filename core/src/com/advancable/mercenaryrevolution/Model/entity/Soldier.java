package com.advancable.mercenaryrevolution.Model.entity;
import com.advancable.mercenaryrevolution.Model.Data;
import com.advancable.mercenaryrevolution.Model.enums.Condition;
import com.advancable.mercenaryrevolution.Model.interfaces.BattleUnit;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author Somkin Ivan
 * Human soldier unit that can carry 3 weapons and have some amount of health, psyche, skill, defense, visibility and mobility.
 */
public class Soldier implements BattleUnit{
    public int health, maxHealth, psyche, maxPsyche, defense, skill,accuracyLose,visibility,mobility,equipped;
    public Weapon[] weapons=new Weapon[3];
    public int[] bullets=new int[3];
    public String name;
    private String getRandomName() {
        return Data.getRandomName();
    }
    private Condition Condition;

    public Soldier(int maxHealth, int health, int maxPsyche, int psyche, int defense, int skill, Weapon[] weapons,int[] bullets, int visibility, int mobility) {
        name = getRandomName();
        this.maxHealth = maxHealth;
        this.health = health;
        this.maxPsyche = maxPsyche;
        this.psyche = psyche;
        this.defense=defense;
        this.skill = skill;
        this.weapons=weapons;
        this.bullets=bullets;
        this.visibility=visibility;
        this.accuracyLose=0;
        this.mobility=mobility;
        equipped=0;
    }

    /**
     * @return Returns a multiplier for a hit damage (0 , 1 or 2)
     */
    @Override
    public int CriticalHit() {
        return 0;
    }
    /**
    *@return Returns a damage to enemy health dealen by equipped weapon
     */
    @Override
    public int DMG_HP() {
        return 0;
    }
    /**
     *@return Returns a damage to enemy psyche dealen by equipped weapon
     */
    @Override
    public int DMG_PSYCHE() {
        return 0;
    }

    /**
     * Analyses the damage dealen by battle unit to the soldier
     *@param battleUnit Battle unit attacking the soldier
     */
    @Override
    public void gotShot(BattleUnit battleUnit) {

    }

    /**
     * Makes soldier run into the cover that protects him from enemy fire.
     * Soldier can't attack enemies while in cover.
     */
    @Override
    public void hide() {

    }

    /**
     * Makes soldier run out of cover
     */
    @Override
    public void show() {

    }

    /**
     * @return Returns psyche amount of the soldier.
     */
    @Override
    public int PSYCHE() {
        return 0;
    }

    @Override
    public void changeWeapon() {

    }
    /**
     * @return Returns health amount of the soldier.
     */
    @Override
    public int HP() {
        return 0;
    }

    /**
     * @return Returns unit card actor.
     */
    @Override
    public Actor getUnitCard() {
        return null;
    }

    /**
     * Makes soldier reload his equipped weapon.
     */
    public void reload(){
        if (bullets[equipped] > 0 && weapons[equipped].clip !=  weapons[equipped].maxClipSize)
            weapons[equipped].clip =bullets[equipped] > weapons[equipped].maxClipSize ? weapons[equipped].maxClipSize : bullets[equipped];
        bullets[equipped] -= weapons[equipped].clip;

    }

    /**
     * @return Returns soldier condition.
     */
    @Override
    public Condition getCondition() {
        return Condition;
    }

    /**
     * @return Returns weapon that the soldier have equipped.
     */
    @Override
    public Weapon getCurrentWeapon() {
        return weapons[equipped];
    }

    /**
     * @return Returns amount of the soldier's visibility
     */
    @Override
    public int Visibility() {
        return 50;
    }
}