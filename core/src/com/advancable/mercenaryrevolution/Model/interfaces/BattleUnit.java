package com.advancable.mercenaryrevolution.Model.interfaces;

import com.advancable.mercenaryrevolution.Model.entity.Weapon;
import com.advancable.mercenaryrevolution.Model.enums.Condition;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Unit that can take part in battle with his statistics and abilities.
 */
public interface BattleUnit {
        int CriticalHit();
        int DMG_HP();
        int DMG_PSYCHE();
        void gotShot(BattleUnit battleUnit);
        void hide();
        void show();
        int HP();
        int PSYCHE();
        void changeWeapon();
        Actor getUnitCard();
        void reload();
        Condition getCondition();
        Weapon getCurrentWeapon();
        int Visibility();

}
