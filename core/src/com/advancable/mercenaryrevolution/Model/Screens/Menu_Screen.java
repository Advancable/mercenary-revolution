package com.advancable.mercenaryrevolution.Model.Screens;

import com.advancable.mercenaryrevolution.Controller.GoToListeners.HelpListener;
import com.advancable.mercenaryrevolution.Controller.GoToListeners.LoadGameSelectionListener;
import com.advancable.mercenaryrevolution.Controller.GoToListeners.NewGameSelectionListener;
import com.advancable.mercenaryrevolution.Controller.GoToListeners.OptionsListener;
import com.advancable.mercenaryrevolution.THEGame;
import com.advancable.mercenaryrevolution.View.ButtonActor;
import com.advancable.mercenaryrevolution.View.RectangleActor;
import com.advancable.mercenaryrevolution.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Screen that holds main menu buttons.
 */
public class Menu_Screen implements Screen {
    private Stage stage;
    RectangleActor background;
    ButtonActor newGame;
    ButtonActor options;
    ButtonActor help;
    ButtonActor loadGame;
    TextureActor title;
    private void addListeners(){
        newGame.addListener(new NewGameSelectionListener());
        loadGame.addListener(new LoadGameSelectionListener());
        options.addListener(new OptionsListener());
        help.addListener(new HelpListener());
    }
    public Menu_Screen(SpriteBatch batch) {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        background = new RectangleActor(0, 126, 1366, 516, 1, 0.78f, 0, 1);
        title = new TextureActor(new TextureRegion(new Texture(String.format("%s%s", THEGame.getAssetsPath(),"textures\\Title.png"))),224,480,906,51);
        TextureRegion menuButtons = new TextureRegion(THEGame.getButtonsTexture(), 0, 0, 944, 612);
        newGame = new ButtonActor(new TextureRegion(menuButtons, 0, 520, 460, 90), new TextureRegion(menuButtons, 460, 520, 460, 90), 446, 325, 460, 90);
        loadGame = new ButtonActor(new TextureRegion(menuButtons, 0, 430, 460, 90), new TextureRegion(menuButtons, 460, 430, 460, 90), 446, 215, 460, 90);
        options = new ButtonActor(new TextureRegion(menuButtons, 0, 30, 108, 200), new TextureRegion(menuButtons, 108, 30, 108, 200), 318, 215, 108, 200);
        help = new ButtonActor(new TextureRegion(menuButtons, 0, 230, 108, 200), new TextureRegion(menuButtons, 108, 230, 108, 200), 926, 215, 108, 200);
        addListeners();
        stage.addActor(background);
        stage.addActor(newGame);
        stage.addActor(loadGame);
        stage.addActor(options);
        stage.addActor(help);
        stage.addActor(title);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
