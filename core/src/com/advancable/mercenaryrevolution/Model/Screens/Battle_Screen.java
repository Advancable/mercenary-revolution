package com.advancable.mercenaryrevolution.Model.Screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Screen that holds an action part of the match.
 */
public class Battle_Screen implements Screen {
    public Battle_Screen(Batch batch){

    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}