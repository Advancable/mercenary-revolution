package com.advancable.mercenaryrevolution.Model.Screens;

import com.advancable.mercenaryrevolution.Controller.GoToListeners.MenuListener;
import com.advancable.mercenaryrevolution.THEGame;
import com.advancable.mercenaryrevolution.View.ButtonActor;
import com.advancable.mercenaryrevolution.View.RectangleActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Screen that holds information about game mechanics.
 */
public class Help_Screen implements Screen {
    private Stage stage;
    RectangleActor background;
    ButtonActor MainMenu;
    private void addListeners(){
        MainMenu.addListener(new MenuListener());
    }
    public Help_Screen(Batch batch){
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        background = new RectangleActor(0, 126, 1366, 516, 1, 0.78f, 0, 1);
        MainMenu = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 216, 76, 326, 84), new TextureRegion(THEGame.getButtonsTexture(), 542, 76, 326, 84), 524, 110, 325, 90);
        addListeners();
        stage.addActor(background);
        stage.addActor(MainMenu);
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}