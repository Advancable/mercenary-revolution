package com.advancable.mercenaryrevolution.Model.Screens;


import com.advancable.mercenaryrevolution.Controller.GoToListeners.MenuListener;
import com.advancable.mercenaryrevolution.Controller.NewGameSelection.SandboxListener;
import com.advancable.mercenaryrevolution.Controller.NewGameSelection.ScenariosListener;
import com.advancable.mercenaryrevolution.Controller.NewGameSelection.CampaignListener;
import com.advancable.mercenaryrevolution.THEGame;
import com.advancable.mercenaryrevolution.View.ButtonActor;
import com.advancable.mercenaryrevolution.View.RectangleActor;
import com.advancable.mercenaryrevolution.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Screen that holds the selection of the new game type and the button to start it.
 */
public class New_Game_Selection_Screen implements Screen {
    public static ButtonActor start;
    Stage stage;
    RectangleActor background;
    ButtonActor campaign;
    ButtonActor sandbox;
    ButtonActor MainMenu;
    ButtonActor scenarios;

    Actor frame;
    TextureActor Choose_the_game_type;
    TextureActor Text;
    public void addText(Texture texture){
        if (Text!=null)
        Text.changeTexture(new TextureRegion(texture));
        else {
            Text = new TextureActor(new TextureRegion(texture), 588, 346, 576, 160);
            stage.addActor(Text);
        }
        Text.setVisible(true);


    }
    public static void setStart(InputListener listener){
        New_Game_Selection_Screen.start.setVisible(true);
        New_Game_Selection_Screen.start.clearListeners();
        New_Game_Selection_Screen.start.addListener(listener);
    }
    public void clear(){
        stage.act(2);
    }
    private void addListeners(){
        campaign.addListener(new CampaignListener());
        scenarios.addListener(new ScenariosListener());
        sandbox.addListener(new SandboxListener());
        MainMenu.addListener(new MenuListener());
    }
    public New_Game_Selection_Screen(SpriteBatch batch) {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        background = new RectangleActor(0, 126, 1366, 516, 1, 0.78f, 0, 1);

        campaign = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 216, 340, 364, 90), new TextureRegion(THEGame.getButtonsTexture(), 580, 340, 364, 90), 195, 442, 363, 90);
        scenarios = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 216, 250, 364, 90), new TextureRegion(THEGame.getButtonsTexture(), 580, 250, 364, 90), 195, 336, 363, 90);
        scenarios = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 944, 0, 364, 90),new TextureRegion(THEGame.getButtonsTexture(), 944, 0, 364, 90),195,336,363,90);
        sandbox = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 216, 160, 364, 90), new TextureRegion(THEGame.getButtonsTexture(), 580, 160, 364, 90), 195, 229, 363, 90);
        sandbox = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 944, 90, 364, 90),new TextureRegion(THEGame.getButtonsTexture(), 944, 90, 364, 90),195,229,363,90);
        MainMenu = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 216, 76, 326, 84), new TextureRegion(THEGame.getButtonsTexture(), 542, 76, 326, 84), 524, 110, 325, 90);
        start = new ButtonActor(new TextureRegion(THEGame.getButtonsTexture(), 216, 0, 342, 76), new TextureRegion(THEGame.getButtonsTexture(), 558, 0, 342, 76), 696, 254, 366, 77);
        addListeners();

        frame = new Actor() {
            ShapeRenderer shape = THEGame.getInstance().getShape();

            @Override
            public void draw(Batch batch, float parentAlpha) {
                batch.end();
                shape.begin(ShapeRenderer.ShapeType.Filled);
                shape.setProjectionMatrix(batch.getProjectionMatrix());
                shape.setTransformMatrix(batch.getTransformMatrix());
                shape.setColor(com.badlogic.gdx.graphics.Color.BLACK);
                shape.rect(579 * THEGame.ppuX, 229 * THEGame.ppuY, 590 * THEGame.ppuX, 8 * THEGame.ppuY);
                shape.rect(579 * THEGame.ppuX, 529 * THEGame.ppuY, 594 * THEGame.ppuX, 8 * THEGame.ppuY);
                shape.rect(1165 * THEGame.ppuX, 229 * THEGame.ppuY, 8 * THEGame.ppuX, 305 * THEGame.ppuY);
                shape.rect(579 * THEGame.ppuX, 229 * THEGame.ppuY, 8 * THEGame.ppuX, 305 * THEGame.ppuY);
                shape.end();
                batch.begin();
            }

        };
        frame.setPosition(0, 0);

        Choose_the_game_type = new TextureActor(new TextureRegion(new Texture(String.format("%s%s%s%s", THEGame.getAssetsPath(), "textures\\", THEGame.Language, "\\Choose_the_game_type.png"))),426,548,506,48);
        stage.addActor(background);
        stage.addActor(campaign);
        stage.addActor(scenarios);
        stage.addActor(sandbox);
        stage.addActor(MainMenu);
        stage.addActor(start);
        stage.addActor(frame);
        stage.addActor(Choose_the_game_type);


    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
        start.setVisible(false);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        if (Text!=null)
        Text.setVisible(false);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}