package com.advancable.mercenaryrevolution.Model.enums;

/**
 * Variable describing what is the unit doing.
 */
public enum Condition {
    IDLE,
    SHOOTING,
    RELOADING,
    PROTECTED,
    SLEEPING,
    HIT,
    DEAD,

}
