package com.advancable.mercenaryrevolution.Model.enums;

/**
 * Type of weapon that decides where it can be put (soldier inventory, vehicles etc.).
 */
public enum WeaponType {
    PISTOL,
    SUBMACHINE_GUN,
    SHOTGUN,
    ASSAULT_RIFLE,
    SNIPER_RIFLE,
    LIGHT_MACHINE_GUN,
    MEDIUM_MACHINE_GUN,
    HEAVY_MACHINE_GUN,
    GRENADE_LAUNCHER,
    ROCKET_LAUNCHER,
    CANNON
}
