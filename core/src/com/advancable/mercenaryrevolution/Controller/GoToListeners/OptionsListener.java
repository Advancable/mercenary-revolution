package com.advancable.mercenaryrevolution.Controller.GoToListeners;

import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Timer;


public class OptionsListener extends InputListener {
    public  Screen screen;
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        event.getTarget().act(1);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s",THEGame.getAssetsPath(),"sounds\\menu button sound.mp3")));
        sound.play();
        if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {

            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    THEGame.getInstance().ShowOptions();
                }

            }, 0.25f);
        }else{
            event.getTarget().act(2);
        }

    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true))
            event.getTarget().act(1);
        else
            event.getTarget().act(2);
    }
}