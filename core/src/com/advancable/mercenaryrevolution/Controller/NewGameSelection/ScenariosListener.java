package com.advancable.mercenaryrevolution.Controller.NewGameSelection;


import com.advancable.mercenaryrevolution.Model.Screens.New_Game_Selection_Screen;
import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Timer;

public class ScenariosListener extends InputListener {
    @Override
    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        event.getTarget().getParent().act(2);
        event.getTarget().act(1);
        return true;
    }
    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button){
        Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s",THEGame.getAssetsPath(),"sounds\\menu button sound.mp3")));
        sound.play();
        Timer timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                THEGame.newGameSelectionScreen.clear();
            }

        }, 0.25f);

        if(event.getTarget()==event.getListenerActor().hit(x,y,true)) {
            THEGame.newGameSelectionScreen.addText(new Texture(String.format("%s%s%s%s", THEGame.getAssetsPath(), "textures\\", THEGame.Language, "\\Scenarios.png")));
            New_Game_Selection_Screen.setStart(new ScenarioApproveListener());
        }

    }
    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true))
            event.getTarget().act(1);
        else
            event.getTarget().act(2);
    }
}