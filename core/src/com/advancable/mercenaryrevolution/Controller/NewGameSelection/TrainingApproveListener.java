package com.advancable.mercenaryrevolution.Controller.NewGameSelection;


/*import com.advancable.mercenaryrevolution.Model.BattleUnit;
import com.advancable.mercenaryrevolution.Model.Data;
import com.advancable.mercenaryrevolution.Model.Match;
import com.advancable.mercenaryrevolution.Model.base;
import com.advancable.mercenaryrevolution.Model.Bullet;
import com.advancable.mercenaryrevolution.Model.soldier;
import com.advancable.mercenaryrevolution.Model.weapon;
import com.advancable.mercenaryrevolution.Model.weaponType;*/
import com.advancable.mercenaryrevolution.THEGame;
//import com.advancable.mercenaryrevolution.View.Soldier;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class TrainingApproveListener extends InputListener {
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        event.getTarget().act(1);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s",THEGame.getAssetsPath(),"sounds\\menu button sound.mp3")));
        sound.play();
        if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {
            /*
            Data.ammo = new Bullet[]{new Bullet("Tranquilizer", 0, 100, 0, 0, 4, 3), new Bullet("9mm", 150, 50, 0, 0, 4, 4)};
            THEGame.currentMatch = new Match(1000000, 60, 100, 5, new Bullet[]{Data.setBullet(Data.ammo[0], 20, 20), Data.setBullet(Data.ammo[1], 100, 100)}, new weapon[][]{new weapon[]{}, new weapon[]{}}, new weapon[][]{new weapon[]{}, new weapon[]{}}, new base[]{}, null, null);

            THEGame.currentMatch.bases = new base[]{new base("Landing Zone", false, 0, new int[]{4, 0, 4, 0, 4}, new int[]{100, 0, 120, 0, 4}, new BattleUnit[]{
                    new Soldier(new soldier(500, 500, 300, 300, 5, 0, new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 5, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true),new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), null, null, true, 45, 50), true),
                    new Soldier(new soldier(500, 500, 300, 300, 5, 5, new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50), true),
                    null,
                    new Soldier(new soldier(500, 500, 300, 300, 5, 10, new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50), true),
                    new Soldier(new soldier(500, 500, 300, 300, 5, 15, new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50), true),
                    new Soldier(new soldier(500, 500, 300, 300, 5, 20,  new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 5, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true),new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), null, null, true, 30, 50), true)})};
            THEGame.currentMatch.Soldiers = new soldier[]{new soldier(500, 500, 300, 300, 5, 20, new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 5, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), new weapon("TrP", weaponType.PISTOL, 0.01f, 80, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50),
                    null,
                    new soldier(500, 500, 300, 300, 5, 5, new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50),
                    new soldier(500, 500, 300, 300, 5, 10, new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 5, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), null, null, true, 30, 50),
                    new soldier(500, 500, 300, 300, 5, 15, new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50),
                    new soldier(500, 500, 300, 300, 5, 0, new weapon("LSAP", weaponType.PISTOL, 0.1f, 50, 8, 5, 2, 0.25f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[1], false), new weapon("TrP", weaponType.PISTOL, 0.01f, 50, 1, 2, 2, 0.3f, 4, 2.93f, 15, true, true, true, THEGame.currentMatch.Bullets[0], true), null, null, true, 30, 50)};
            Battle_Screen.Background = new Texture(THEGame.getAssetsPath() + "locations/Snowy Mountains.png");
            Battle_Screen.Your = new BattleUnit[]{
                    new Soldier(THEGame.currentMatch.Soldiers[0], false),
                    null,
                    new Soldier(THEGame.currentMatch.Soldiers[2], false),
                    new Soldier(THEGame.currentMatch.Soldiers[3], false),
                    new Soldier(THEGame.currentMatch.Soldiers[4], false),
                    new Soldier(THEGame.currentMatch.Soldiers[5], false)};
            Battle_Screen.Enemy = new BattleUnit[]{
                    THEGame.currentMatch.bases[0].DefendingSquad[0],
                    THEGame.currentMatch.bases[0].DefendingSquad[1],
                    null,
                    THEGame.currentMatch.bases[0].DefendingSquad[3],
                    THEGame.currentMatch.bases[0].DefendingSquad[4],
                    THEGame.currentMatch.bases[0].DefendingSquad[5]};
*/
            THEGame.getInstance().setScreen(THEGame.battleScreen);
        }else{
            event.getTarget().act(2);
        }
    }
    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true))
            event.getTarget().act(1);
        else
            event.getTarget().act(2);
    }

}