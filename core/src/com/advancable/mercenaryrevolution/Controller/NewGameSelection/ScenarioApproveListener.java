package com.advancable.mercenaryrevolution.Controller.NewGameSelection;


import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class ScenarioApproveListener extends InputListener {
    @Override
    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        event.getTarget().act(1);
        return true;
    }
    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button){
        event.getTarget().act(2);


    }
    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true))
            event.getTarget().act(1);
        else
            event.getTarget().act(2);
    }
}