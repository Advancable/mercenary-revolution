package com.advancable.mercenaryrevolution.View;

import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Actor with texture. Simple.
 */
public class TextureActor extends Actor {
    public TextureRegion img;

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }

    public TextureActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * THEGame.ppuX, y * THEGame.ppuY);
        setSize(width * THEGame.ppuX, height * THEGame.ppuY);
    }
    public void changeTexture(TextureRegion img){
        this.img = img;
    }
}
