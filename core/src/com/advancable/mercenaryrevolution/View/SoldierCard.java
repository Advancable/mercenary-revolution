package com.advancable.mercenaryrevolution.View;

import com.advancable.mercenaryrevolution.Model.Data;
import com.advancable.mercenaryrevolution.Model.entity.Soldier;
import com.advancable.mercenaryrevolution.Model.enums.Condition;
import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author Somkin Ivan
 * Soldier view class used as unit card in battle.
 */
public class SoldierCard extends Actor {
    private Soldier soldier;
    private boolean isEnemy;
    private Animation Idle, Shot, Reload, Hit, Death;
    private float animationStart;
    private float randomNumber;
    /**
     * Drawing black outlines used as background for the name, skill points, health bar and psyche bar.
     */
    private void drawOutlines() {
        THEGame.getInstance().getShape().begin(ShapeRenderer.ShapeType.Filled);
        THEGame.getInstance().getShape().setColor(0, 0, 0, 1);
        THEGame.getInstance().getShape().rect(getX(), (getY() + 142 * THEGame.ppuY), 295 * THEGame.ppuX, 28 * THEGame.ppuY);
        THEGame.getInstance().getShape().end();
        THEGame.getInstance().getShape().begin(ShapeRenderer.ShapeType.Filled);
        THEGame.getInstance().getShape().setColor(0, 0, 0, 1);
        THEGame.getInstance().getShape().rect(getX(), getY(), 295 * THEGame.ppuX, 39 * THEGame.ppuY);
        THEGame.getInstance().getShape().end();
    }
    /**
     * Drawing unit card background regarding his fighting side or physical condition.
     */
    private void drawBackground() {
        THEGame.getInstance().getShape().begin(ShapeRenderer.ShapeType.Filled);
        if (soldier.getCondition() == Condition.DEAD)
            THEGame.getInstance().getShape().setColor(0.5f, 0.5f, 0.5f, 1);
        else if (soldier.getCondition() == Condition.SLEEPING)
            THEGame.getInstance().getShape().setColor(0.7f, 0.7f, 1, 1);
        else if (!isEnemy)
            THEGame.getInstance().getShape().setColor(1, 0.78f, 0, 1);
        else
            THEGame.getInstance().getShape().setColor(0.57f, 0.16f, 0.47f, 1);

        THEGame.getInstance().getShape().rect(getX(), (getY() + 38 * THEGame.ppuY), 295 * THEGame.ppuX, 106 * THEGame.ppuY);
        THEGame.getInstance().getShape().end();
    }
    /**
     * Drawing health bar.
     */
    private void drawHealth(Batch batch) {
        batch.draw(new TextureRegion(Data.getUnitIcons(), 0, 36, 23, 15), (getX() + 2 * THEGame.ppuX), (getY() + 20 * THEGame.ppuY), 23 * THEGame.ppuX, 15 * THEGame.ppuY);
        THEGame.getInstance().getShape().begin(ShapeRenderer.ShapeType.Filled);
        THEGame.getInstance().getShape().setColor(1, 1, 1, 1);
        THEGame.getInstance().getShape().rect((getX() + 29 * THEGame.ppuX), (getY() + 22 * THEGame.ppuY), 260 * ((float) soldier.health / (float) soldier.maxHealth) * THEGame.ppuX, 9 * THEGame.ppuY);
        THEGame.getInstance().getShape().end();
    }
    /**
     * Drawing psyche bar.
     */
    private void drawPsyche(Batch batch) {
        batch.draw(new TextureRegion(Data.getUnitIcons(), 0, 20, 70, 16), (getX() + 2 * THEGame.ppuX), (getY() + 1 * THEGame.ppuY), 70 * THEGame.ppuX, 16 * THEGame.ppuY);
        THEGame.getInstance().getShape().begin(ShapeRenderer.ShapeType.Filled);
        THEGame.getInstance().getShape().setColor(196f / 256f, 196f / 256f, 196f / 256f, 1);
        THEGame.getInstance().getShape().rect((getX() + 74 * THEGame.ppuX), (getY() + 5 * THEGame.ppuY), 215 * ((float) soldier.psyche / (float) soldier.maxPsyche) * THEGame.ppuX, 8 * THEGame.ppuY);
        THEGame.getInstance().getShape().end();
    }
    /**
     * Drawing idle animation.
     */
    private void drawIdle(Batch batch) {
        if (isEnemy)
            batch.draw(Idle.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f + randomNumber), getX() + 295 * THEGame.ppuX, getY() + 38 * THEGame.ppuY, getX(), getY(), (-295) * THEGame.ppuX, 105 * THEGame.ppuY, 1, 1, 0);
        else
            batch.draw(Idle.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f + randomNumber), getX(), getY() + 38 * THEGame.ppuY, 295 * THEGame.ppuX, 105 * THEGame.ppuY);
    }
    /**
     * Drawing hit animation.
     */
    private void drawHit(Batch batch) {
        if (isEnemy)
            batch.draw(Hit.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f - animationStart), getX() + 295 * THEGame.ppuX, getY() + 38 * THEGame.ppuY, getX(), getY(), (-295) * THEGame.ppuX, 105 * THEGame.ppuY, 1, 1, 0);
        else
            batch.draw(Hit.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f - animationStart), getX(), getY() + 38 * THEGame.ppuY, 295 * THEGame.ppuX, 105 * THEGame.ppuY);
    }
    /**
     * Drawing shooting animation.
     */
    private void drawShooting(Batch batch) {
        if (isEnemy)
            batch.draw(Shot.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f - animationStart), getX() + 295 * THEGame.ppuX, getY() + 38 * THEGame.ppuY, getX(), getY(), (-295) * THEGame.ppuX, 105 * THEGame.ppuY, 1, 1, 0);
        else
            batch.draw(Shot.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f - animationStart), getX(), getY() + 38 * THEGame.ppuY, 295 * THEGame.ppuX, 105 * THEGame.ppuY);
    }
    /**
     * Drawing reloading animation.
     */
    private void drawReloading(Batch batch) {
        if (isEnemy)
            batch.draw(Reload.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f - animationStart), getX() + 295 * THEGame.ppuX, getY() + 38 * THEGame.ppuY, getX(), getY(), (-295) * THEGame.ppuX, 105 * THEGame.ppuY, 1, 1, 0);
        else
            batch.draw(Reload.getKeyFrame(System.currentTimeMillis() % 1000000 / 1000f - animationStart), getX(), getY() + 38 * THEGame.ppuY, 295 * THEGame.ppuX, 105 * THEGame.ppuY);
    }
    /**
     * Drawing soldier characteristics (name, skill points, equipped weapon).
     */
    private void drawChars(Batch batch) {
        Data.getUnitNameFont().draw(batch, soldier.name, (getX() + (145 - soldier.name.length() * 6.5f) * THEGame.ppuX), (getY() + 167 * THEGame.ppuY));
        if (soldier.getCondition() != Condition.DEAD) {
            Data.getUnitStatsFont().setColor(Color.CYAN);
            Data.getUnitStatsFont().draw(batch, String.format("%s%d", soldier.skill / 10 > 0 ? String.format("%d", soldier.skill / 10) : "  ", soldier.skill % 10), (getX() + (260) * THEGame.ppuX), (getY() + 165 * THEGame.ppuY));
            Data.getUnitStatsFont().setColor(Color.BLACK);
            if (!isEnemy)
                Data.getUnitStatsFont().draw(batch, String.format("%d/%d", soldier.weapons[soldier.equipped].clip, soldier.bullets[soldier.equipped]), (getX() + (10) * THEGame.ppuX), (getY() + 60 * THEGame.ppuY));
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        drawOutlines();
        drawBackground();
        drawHealth(batch);
        drawPsyche(batch);
        batch.begin();
        if (soldier.getCondition() == Condition.IDLE) {
            drawIdle(batch);
        }
        if (soldier.getCondition() == Condition.HIT) {
            drawHit(batch);
        }
        if (soldier.getCondition() == Condition.SHOOTING) {
            drawShooting(batch);
        }
        if (soldier.getCondition() == Condition.RELOADING) {
            drawReloading(batch);
        }
        drawChars(batch);
    }


}
