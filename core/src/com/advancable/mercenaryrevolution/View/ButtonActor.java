package com.advancable.mercenaryrevolution.View;
import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Actor with 2 changing textures.
 */
public class ButtonActor extends Actor{
    private TextureRegion img,img2;
    public boolean toggled;
    @Override
    public void draw(Batch batch,float parentAlpha){
        if (!toggled)
        batch.draw(img,getX(),getY(),getWidth(),getHeight());
        else
        batch.draw(img2,getX(),getY(),getWidth(),getHeight());
    }
    @Override
    public void act(float delta){
        toggled = delta == 1;
    }
    public ButtonActor(TextureRegion img, TextureRegion img2, float x, float y, float width, float height){
        this.img=img;
        this.img2=img2;
        setPosition(x* THEGame.ppuX,y* THEGame.ppuY);
        setSize(width* THEGame.ppuX,height* THEGame.ppuY);
    }
}