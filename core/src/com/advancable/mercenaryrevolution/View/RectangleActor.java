package com.advancable.mercenaryrevolution.View;
import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;


/**
 * Just a rectangle.
 */
public class RectangleActor extends Actor{
    public Color color;
    @Override
    public void draw(Batch batch,float parentAlpha){
        batch.end();
        ShapeRenderer shape= THEGame.getInstance().getShape();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(color);
        shape.rect(getX()*THEGame.ppuX,getY()*THEGame.ppuY,getWidth()*THEGame.ppuX,getHeight()*THEGame.ppuY);
        shape.end();
        batch.begin();
    }
    public RectangleActor(float x, float y, float width, float height, float r, float g, float b, float a){
        setPosition(x,y);
        setSize(width, height);
        color = new Color(r,g,b,a);
    }

}