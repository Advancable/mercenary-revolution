package com.advancable.mercenaryrevolution.desktop;

import com.advancable.mercenaryrevolution.THEGame;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static LwjglApplicationConfiguration config;
	public static void main (String[] arg) {
		config = new LwjglApplicationConfiguration();
		System.setProperty("user.name", "English");
		new LwjglApplication(THEGame.getInstance(), config);
		config.addIcon("android\\assets\\logos\\logo.jpg", Files.FileType.Internal);
		config.addIcon("android\\assets\\logos\\logo2.jpg", Files.FileType.Internal);
		config.addIcon("android\\assets\\logos\\logo3.jpg", Files.FileType.Internal);
		config.title="Mercenary Revolution";
		config.width=1366;
		config.height=768;
		THEGame.setAssetsPath("android\\assets\\");
		THEGame.Language="eng";
	}
}